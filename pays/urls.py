from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^api/register/$', views.register, name='register'),
    url(r'^api/login', views.login_user, name='login_user'),
    url(r'^api/logout', views.logout_user, name='logout_user'),
    url(r'^api/newaccount$', views.newAccount, name='newAccount'),
    url(r'^api/deposit$', views.deposit, name='deposit'),
    url(r'^api/transfer$', views.transfer, name='transfer'),
    url(r'^api/balance$', views.balance, name='balance'),
    url(r'^api/createinvoice$', views.createinvoice, name='createinvoice'),
    url(r'^api/payinvoice$', views.payinvoice, name='payinvoice'),
    # url(r'^pay-invoice/(?P<)$', views.logout, name='pay-invoice'),
    url(r'^api/statement$', views.statement, name='statement'),
    #url(r'^help$', views.logout, name='logout'),



    # path('<int:acc_number>', views.transaction, name = 'Transactions')

]
