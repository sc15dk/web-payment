from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.utils.crypto import get_random_string
import json
import requests
import random
import datetime
from decimal import Decimal
from .models import Customers, Accounts, Transactions, Invoice
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

# return JsonResponse({'foo':'bar'})

@csrf_exempt
def register(request):
    if request.method == 'GET':
        return HttpResponse("Please, provide information")
    elif request.method == 'POST':
        try:
            data = json.loads(request.body.decode('utf-8'))
            # data=json.loads(request.raw_post_data)
            # data=json.loads(request.body.decode('utf-8'))
            first_name = data['first_name']
            last_name  = data['last_name']
            email = data['email']
            phone_nu = data['phone_nu']
            username = data['username']
            if Customers.objects.filter(username=username).exists():
                return HttpResponse("User Already Exists")
            password = data['password']
            CustomerType = data['CustomerType']
            user = Customers(first_name = first_name,
                             last_name = last_name,
                             email = email,
                             phone_nu = phone_nu,
                             username = username,
                             password = password,
                             CustomerType = CustomerType)
            user.save()
            return HttpResponse("Created - Welcome!", status=201)
        except (KeyError,Exception) as e:
            return HttpResponse('Failed to handle %s' % str(e), status=503)

@csrf_exempt
def login_user(request):
    if request.method == 'GET':
        return HttpResponse("Please, provide information")
    elif request.method == 'POST':

        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            current_user = request.user
            print (current_user.username)
            return HttpResponse("logged in", status=200)
            # Redirect to a success page.
        else:
            # No backend authenticated the credentials
            return HttpResponse("error - to login")

@csrf_exempt
def logout_user(request):
    try:
        current_user = request.user
        print (current_user.username)
        logout(request)
        return HttpResponse("Loged out", status = 200)
    except Exception as e:
        return HttpResponse("Failed - not loged out")

@csrf_exempt
@login_required(login_url='/api/login')
def newAccount(request):
    try:
        if request.user.is_authenticated():
             current_user = request.user
        else:
            return HttpResponse("Log in, please")
        try:
            customer = Customers.objects.get(username = current_user.username)
        except Customers.DoesNotExist:
            return HttpResponse("This customer doesn't exists, please create one")

        activated = True
        if customer.accounts_set.all().exists():
            activated = False
        unique_id = '{0:08}'.format(random.randint(1, 100000))
        a = Accounts(acc_number = unique_id,  customer=customer, balance = 0, activated =activated)
        a.save()
        return HttpResponse("Account created", status = 201)
    except Exception as e:
        return HttpResponse('Failed to handle "%s"' % str(e), status=503)

@csrf_exempt
@login_required(login_url='/api/login')
def deposit(request):
    if request.user.is_authenticated():
        current_user = request.user
    try:
        data = json.loads(request.body.decode('utf-8'))
        # data=json.loads(request.raw_post_data)
        # data=json.loads(request.body.decode('utf-8'))

        amount = data['amount']
        account_num = data['account_num']
        print(account_num)
        try:
            addUP = Accounts.objects.get(acc_number = account_num)
        except Accounts.DoesNotExist:
            return HttpResponse("This account doesn't exists, please create one")
        addUP.balance += Decimal( amount)
        addUP.save()
        return JsonResponse({'account_num': account_num, 'balance': addUP.balance }, status = 201)
    except (KeyError,Exception) as e:
        return HttpResponse('Failed to handle "%s"' % str(e), status=503)


@csrf_exempt
@login_required(login_url='/api/login')
def transfer(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
        # data=json.loads(request.raw_post_data)
        # data=json.loads(request.body.decode('utf-8'))
        amount = Decimal(data['amount'])
        from_account_num = data['from_account_num']
        to_account_num = data['to_account_num']
        try:
            accountFr = Accounts.objects.get(acc_number = from_account_num)
            accountTo = Accounts.objects.get(acc_number = to_account_num)
        except Account.DoesNotExist:
            return HttpResponse("This account doesn't exists, please create one")
        if accountFr.balance < Decimal(amount):
            return HttpResponse("Please, deposit your account")
        accountFr.balance -= Decimal(amount)
        accountFr.save()

        accountTo.balance += Decimal(amount)
        accountTo.save()
        ref_num = '{0:08}'.format(random.randint(1, 100000))
        trns = Transactions(transaction_date = datetime.datetime.today(),
                acc_from = accountFr,
                acc_to = accountTo,
                ref_num = ref_num,
                amount = amount)
        trns.save()
        return JsonResponse({'account_num': from_account_num, 'balance': accountFr.balance }, status = 201)
    except (KeyError,Exception) as e:
        return HttpResponse('Failed to handle "%s"' % str(e), status=503)





@csrf_exempt
@login_required(login_url='/api/login')
def balance(request):
    if request.method == 'POST':
        return HttpResponse("POST unavailable")
    elif request.method == 'GET':
        if request.user.is_authenticated():
             current_user = request.user
             print(current_user.username)
        else:
            return HttpResponse("Log in, please")
        try:
            customer = Customers.objects.get(username = current_user.username)
        except Customers.DoesNotExist:
            return HttpResponse("This accouts don't exist, please create one")
        accounts = customer.accounts_set.all()
        data = {'accounts': []}
        for acc in accounts:
            data['accounts'].append({'acc_number': acc.acc_number, 'balance': acc.balance})
        # return HttpResponse("", data=data, content-type="application/json")
        return JsonResponse(data,  status = 201)



    #return HttpResponse("Hello world")

@csrf_exempt
@login_required(login_url='/api/login')
def createinvoice(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
        account_num = data['account_num']
        client_ref_num = data['client_ref_num']
        amount = data['amount']
        try:
            accountFr = Accounts.objects.get(acc_number = account_num)
        except Account.DoesNotExist:
            return HttpResponse("This account doesn't exists, please create one")
        inv_number = '{0:08}'.format(random.randint(1, 100000))
        elec_stamp = get_random_string(length=10)
        inv = Invoice( inv_number = inv_number,
        invoice_reciever = accountFr,
        payee_reference = client_ref_num,
        amount = Decimal(amount),
        payment_status = False,
        elec_stamp = elec_stamp)
        inv.save()

        return JsonResponse({'payprovider_ref_num': inv_number, 'stamp_code': elec_stamp }, status = 201)
    except (KeyError,Exception) as e:
        return HttpResponse('Failed to handle "%s"' % str(e), status=503)




@csrf_exempt
@login_required(login_url='/api/login')
def payinvoice(request):
    try:
        data = json.loads(request.body.decode('utf-8'))

        payprovider_ref_num = data['payprovider_ref_num']
        client_ref_num = data['client_ref_num']
        amount = data['amount']
        if request.user.is_authenticated():
             current_user = request.user
        else:
            return HttpResponse("Log in, please")

        try:
            customer = Customers.objects.get(username = request.user.username)
        except Customers.DoesNotExist:
            return HttpResponse("This customer doesn't exists, please create one")

        invoice = Invoice.objects.get(inv_number = payprovider_ref_num)
        accountGets = invoice.invoice_reciever
        print(accountGets.acc_number)
        accountPays = Accounts.objects.get(customer = customer, activated = True)

        if accountPays.balance < Decimal(amount):
            return HttpResponse("Please, deposit your account")

        accountPays.balance -= Decimal(amount)
        accountPays.save()
        accountGets.balance += Decimal(amount)
        accountGets.save()
        invoice.payment_status = True
        invoice.invoice_sender = accountPays
        invoice.payment_date = datetime.datetime.today()
        invoice.save()


        return JsonResponse({'stamp_code': invoice.elec_stamp }, status = 201)
    except (KeyError,Exception) as e:
        return HttpResponse('Failed to handle "%s"' % str(e), status=503)



@csrf_exempt
@login_required(login_url='/api/login')
def statement(request):
    try:
        data = json.loads(request.body.decode('utf-8'))
        # data=json.loads(request.raw_post_data)
        # data=json.loads(request.body.decode('utf-8'))
        account_num = data['account_num']
        if request.user.is_authenticated():
             current_user = request.user
        else:
            return HttpResponse("Log in, please")
        try:
            customer = Customers.objects.get(username = current_user.username)
        except Customers.DoesNotExist:
            return HttpResponse("This customer doesn't exists, please create one")
        #Check if the customer has the account
        print('2')

        if Accounts.objects.filter(customer = customer).count() > 0:
            print('2')
            account = Accounts.objects.filter(acc_number = account_num)
            transactions = Transactions.objects.filter(acc_to = account)
            data = {'transactions': []}
            for transaction in transactions:
                data['transactions'].append({'date': transaction.transaction_date, 'reference': transaction.ref_num, 'amount': -transaction.amount})
            transactions = Transactions.objects.filter(acc_from = account)
            for transaction in transactions:
                data['transactions'].append({'date': transaction.transaction_date, 'reference': transaction.ref_num, 'amount': transaction.amount})

        return JsonResponse(data, status = 201)
    except (KeyError,Exception) as e:
        return HttpResponse('Failed to handle "%s"' % str(e), status=503)
