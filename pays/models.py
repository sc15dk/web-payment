from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Customers(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=40)
    email = models.EmailField()
    phone_nu = models.CharField(max_length=15)
    username = models.CharField(max_length=40, unique = True)
    password = models.CharField(max_length=40)
    CustomerTypes = [ ('personal', 'Personal Traveler'), ('business', 'Business Traveler') ]
    CustomerType = models.CharField (max_length = 32 , choices = CustomerTypes , default = 'unknown')
    # CustomerCode = models.CharField (max_length = 8 , unique = True)

    # authors = models.ManyToManyField(author)
    # publisher = models.ForeignKey(first_name)


    # def send_money(self):
    #     if self.first_name = "Bill Gates":
    #         return "smart"
    def __str__(self):
        return '%s %s: %s' % (self.first_name, self.last_name, self.CustomerType)

class Accounts(models.Model):
    acc_number = models.CharField(max_length = 8, unique = True)
    customer = models.ForeignKey(Customers, on_delete=models.CASCADE)
    balance = models.DecimalField(max_digits=12, decimal_places=2,null=True)
    activated = models.BooleanField(default=False)

class Transactions(models.Model):
    transaction_date = models.DateField()
    acc_from = models.ForeignKey(Accounts,related_name='request_created', on_delete=models.PROTECT)
    acc_to = models.ForeignKey(Accounts, related_name='request_assigned',on_delete=models.PROTECT)
    ref_num = models.CharField(max_length = 8, unique = True)  #optional make Maybe make UNIQUE
    amount = models.DecimalField(max_digits=12, decimal_places=2)

class Invoice(models.Model):
    inv_number = models.CharField(max_length = 8, unique = True)
    invoice_reciever = models.ForeignKey(Accounts, related_name='invoice_reciever',on_delete=models.PROTECT)
    payee_reference = models.CharField(max_length = 8, unique = True)
    amount = models.DecimalField(max_digits=12, decimal_places=2)
    payment_status = models.BooleanField(default=False)
    invoice_sender = models.ForeignKey(Accounts, related_name='invoice_sender',on_delete=models.PROTECT, null = True)
    payment_date = models.DateField(null = True)
    elec_stamp = models.CharField(max_length = 10, unique = True)
